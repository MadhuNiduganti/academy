package com.CourseReg.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class UserEnrollment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int enrollmentId;
	private String courseCode;
	private String courseName;
	private LocalDateTime startDate;
	private LocalDateTime endDate;
	private int userId;
	private String status;
}
