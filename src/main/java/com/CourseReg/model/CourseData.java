package com.CourseReg.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class CourseData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int courseDataId;
	private String courseCode;
	private String courseName;
	
}
