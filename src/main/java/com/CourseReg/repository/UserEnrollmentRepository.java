package com.CourseReg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


import com.CourseReg.model.UserEnrollment;

public interface UserEnrollmentRepository extends JpaRepository<UserEnrollment, Integer>{

	
	List<UserEnrollment> findAllByUserIdOrderByStatus(Integer id);
}
