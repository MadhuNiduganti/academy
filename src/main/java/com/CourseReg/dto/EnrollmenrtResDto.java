package com.CourseReg.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class EnrollmenrtResDto {
	
	private String message;
	private String statusCode;
	
	private List<EnrollmentDto> enrollmentDtoList;
	

}
