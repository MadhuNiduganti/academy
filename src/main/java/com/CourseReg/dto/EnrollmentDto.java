package com.CourseReg.dto;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class EnrollmentDto {
	
	private int enrollmentId;
	private String courseCode;
	private String courseName;
	private LocalDateTime startDate;
	private LocalDateTime endDate;
	//private int userId;
	private String status;
	

}
