package com.CourseReg.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserResponseDto {
	
	private Integer userId;
	private Integer statusCode;
	private String statusMessage;

}
