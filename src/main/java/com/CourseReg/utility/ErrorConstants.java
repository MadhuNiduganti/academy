package com.CourseReg.utility;

public class ErrorConstants {

	
	public static final String INVALID_INPUT = "Please provide the required  data";
	public static final int INVALID_INPUT_CODE = 602;

	public static final String LOGIN_SUCCESS = "logged in successfully";
	public static final int LOGIN_SUCCESS_CODE = 603;


	public static final String LOGIN_FAIL = "invalid username or password";
	public static final int LOGIN_FAIL_CODE = 604;

}
