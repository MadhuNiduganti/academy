package com.CourseReg.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.CourseReg.dto.EnrollmenrtResDto;
import com.CourseReg.exception.UserNotFoundException;
import com.CourseReg.service.ViewEnrollmentsService;


@RestController
public class ViewEnrollmentsController {
	
	public static final Logger logger=LogManager.getLogger(ViewEnrollmentsController.class.getName());
	
	@Autowired
	ViewEnrollmentsService viewEnrollementsService;
	
	@GetMapping("/users/{userId}/enrollements")
	public ResponseEntity<EnrollmenrtResDto> viewEnrollments(@PathVariable(required=true) Integer userId) 
			throws UserNotFoundException {
		logger.info("started");
		EnrollmenrtResDto enrollmenrtResDto =viewEnrollementsService.findAllByUserIdGroupByStatus(userId);
		return new ResponseEntity(enrollmenrtResDto, HttpStatus.OK);
	}
	
	
	@GetMapping("/users1")
	public String show() 
			throws UserNotFoundException {
		logger.info("started");
	//	EnrollmenrtResDto enrollmenrtResDto =viewEnrollementsService.findAllByUserIdGroupByStatus(userId);
		return "Hello";
	}
}
