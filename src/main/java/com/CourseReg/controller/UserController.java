package com.CourseReg.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.CourseReg.dto.UserRequestDto;
import com.CourseReg.dto.UserResponseDto;
import com.CourseReg.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {
	
	@Autowired
	UserService userService;
	
	/**
	 * This method is used to check the login details of the user
	 * 
	 * @author Madhu
	 * @since 2020-11-28
	 * @param userName  -Here we use userName to check the username correct or not
	 * @param password -Here we use password to check the password correct or not
	 * @return ResponseEntity Object along with status code and success login
	 *         message
	 * 
	 */
	
	@PostMapping("login")
	public ResponseEntity<UserResponseDto> userLogin(@RequestBody UserRequestDto requestDto) {
		UserResponseDto responseDto = userService.userLogin(requestDto);
		return new ResponseEntity<>(responseDto, HttpStatus.OK);
	}

}
