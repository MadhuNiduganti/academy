package com.CourseReg.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class ResponseError {
	
	private String message;
	private int statusCode;
	
	public ResponseError() {
		
	}

	public ResponseError(String message, int statusCode) {
		this.message = message;
		this.statusCode = statusCode;

	}
}
