package com.CourseReg.exception;

import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	  
	  @ExceptionHandler(UserNotFoundException.class)
	  public ResponseEntity<ResponseError> userNotFoundExceptionHandler(UserNotFoundException ex,WebRequest request) {
		  ResponseError error = new ResponseError();
		  error.setMessage(ex.getMessage());
		  error.setStatusCode(404);
	  
	  return new ResponseEntity<>(error,HttpStatus.NOT_FOUND);
	  
	  }
	  
	 
	 

	  @ExceptionHandler(Exception.class)
		public ResponseEntity<ErrorResponse> globalExceptionHandler(Exception exception, WebRequest request) {
			 ErrorResponse errorResponse = new ErrorResponse();
			  errorResponse.setMessage(exception.getMessage());

			return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

		}
	  
	  @Override
		protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception,
				HttpHeaders headers,HttpStatus status, WebRequest request ){
			String allFieldErrors =exception.getBindingResult().getFieldErrors().stream().map(FieldError::getDefaultMessage).collect(Collectors.joining(", "));
			ResponseError error = new ResponseError();
			error.setStatusCode(400);
			error.setMessage(allFieldErrors);
			return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
		}
		

}