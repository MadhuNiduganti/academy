package com.CourseReg.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.CourseReg.dto.EnrollmenrtResDto;
import com.CourseReg.exception.UserNotFoundException;
import com.CourseReg.model.UserEnrollment;

@Service
public interface ViewEnrollmentsService {

	EnrollmenrtResDto findAllByUserIdGroupByStatus(Integer id) throws UserNotFoundException;
}
