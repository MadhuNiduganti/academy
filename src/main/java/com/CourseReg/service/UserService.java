package com.CourseReg.service;

import org.springframework.stereotype.Service;

import com.CourseReg.dto.UserRequestDto;
import com.CourseReg.dto.UserResponseDto;


@Service
public interface UserService {
	
	public UserResponseDto userLogin(UserRequestDto requestDto);
	
}
