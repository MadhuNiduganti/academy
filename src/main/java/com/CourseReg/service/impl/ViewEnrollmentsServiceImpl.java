package com.CourseReg.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.CourseReg.dto.EnrollmenrtResDto;
import com.CourseReg.dto.EnrollmentDto;
import com.CourseReg.exception.UserNotFoundException;
import com.CourseReg.model.User;
import com.CourseReg.repository.UserEnrollmentRepository;
import com.CourseReg.service.ViewEnrollmentsService;
import com.CourseReg.repository.UserRepository;

@Service
public class ViewEnrollmentsServiceImpl<UserEnrollment> implements ViewEnrollmentsService {
	
	@Autowired
	UserEnrollmentRepository userEnrollmentRepository;
	@Autowired
	UserRepository UserRepository;
	
	public EnrollmenrtResDto findAllByUserIdGroupByStatus(Integer id) throws UserNotFoundException{
		
		
		
		EnrollmenrtResDto enrollmenrtResDto = new EnrollmenrtResDto();
		List<EnrollmentDto> enrollmentDtoList = new ArrayList<>();
		
		Optional<User> user=UserRepository.findById(id);
		if(!user.isPresent()) {
			throw new UserNotFoundException("User Not Found");
		}
		
		List<com.CourseReg.model.UserEnrollment> userEnrollmentlist=	userEnrollmentRepository.findAllByUserIdOrderByStatus(id);
		
		userEnrollmentlist.stream().forEach(userEnrollment->{
		EnrollmentDto enrollmentDto = new EnrollmentDto();
		BeanUtils.copyProperties(userEnrollment, enrollmentDto);
		enrollmentDtoList.add(enrollmentDto);
	});
	
	enrollmenrtResDto.setEnrollmentDtoList(enrollmentDtoList);
	enrollmenrtResDto.setStatusCode("200");
	enrollmenrtResDto.setMessage("Success Response");
	return enrollmenrtResDto;
	}

}
