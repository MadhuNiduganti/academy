package com.CourseReg.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.CourseReg.utility.ErrorConstants;
import com.CourseReg.dto.UserRequestDto;
import com.CourseReg.dto.UserResponseDto;
import com.CourseReg.model.User;
import com.CourseReg.repository.UserRepository;
import com.CourseReg.service.UserService;

@Service
public class UserServiceImpl  implements UserService {

	@Autowired
	UserRepository userRepository;


	/**
	 * 
	 * @param requestDto
	 * @return ResponseDto object if user logged in successfully or not with status
	 *         code and message
	 */

	@Override
	public UserResponseDto userLogin(UserRequestDto requestDto) {
		UserResponseDto responseDto = new UserResponseDto();

		if (requestDto.getUserName().isEmpty() || requestDto.getPassword().isEmpty()) {
			responseDto.setStatusCode(ErrorConstants.INVALID_INPUT_CODE);
			responseDto.setStatusMessage(ErrorConstants.INVALID_INPUT);

		} else {
			User user = userRepository.findByUserNameAndPassword(requestDto.getUserName(), requestDto.getPassword());

			if (user == null) {
				
				//responseDto.setUserId(user.getUserId());
				responseDto.setStatusCode(ErrorConstants.LOGIN_FAIL_CODE);
				responseDto.setStatusMessage(ErrorConstants.LOGIN_FAIL);
									
				
			} else {
				responseDto.setUserId(user.getUserId());
				responseDto.setStatusCode(ErrorConstants.LOGIN_SUCCESS_CODE);
				responseDto.setStatusMessage(ErrorConstants.LOGIN_SUCCESS);
			}
		}
		return responseDto;
	}
}
