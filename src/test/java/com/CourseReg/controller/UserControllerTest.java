package com.CourseReg.controller;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.CourseReg.controller.UserController;
import com.CourseReg.dto.UserRequestDto;
import com.CourseReg.dto.UserResponseDto;
import com.CourseReg.service.UserService;
import com.CourseReg.utility.ErrorConstants;


@SpringBootTest
public class UserControllerTest {

	@InjectMocks
	UserController userController;
	
	@Mock
	UserService userService;

	static UserResponseDto  userResponseDto;

	
	@BeforeAll
	public static void setup() {
		userResponseDto = new UserResponseDto();
		userResponseDto.setStatusMessage("test");
		userResponseDto.setStatusCode(200);
	}
	
	@Test
	public void userLoginTest() {
		UserRequestDto userDto = new UserRequestDto();
		userDto.setUserName("abc@gmail.com");
		userDto.setPassword("asdf");

		UserResponseDto responseDto = new UserResponseDto();
		responseDto.setStatusCode(ErrorConstants.INVALID_INPUT_CODE);
		responseDto.setStatusMessage(ErrorConstants.INVALID_INPUT);

		Mockito.when(userService.userLogin(userDto)).thenReturn(responseDto);
		ResponseEntity<UserResponseDto> result = userController.userLogin(userDto);
		assertEquals(HttpStatus.OK, result.getStatusCode());
	}

}
