package com.CourseReg.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.time.LocalDateTime;
import java.util.ArrayList;
//import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
//import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.CourseReg.dto.EnrollmenrtResDto;
import com.CourseReg.dto.EnrollmentDto;
import com.CourseReg.exception.GlobalExceptionHandler;
import com.CourseReg.exception.ResponseError;
import com.CourseReg.exception.UserNotFoundException;
import com.CourseReg.service.ViewEnrollmentsService;

//@ContextConfiguration
@SpringBootTest
//@ExtendWith(MockitoExtension.class)
@TestInstance(Lifecycle.PER_CLASS)
public class ViewEnrollmentsControllerTest {
	
	
	ResponseError responseError;
	EnrollmenrtResDto responseDto;
	@BeforeAll
	public  void setup() {
		
		List<EnrollmentDto> enrollmentDtoList = new ArrayList();
		EnrollmentDto enrollmentDto = new EnrollmentDto(); 
		enrollmentDto.setCourseCode("JAVA01");
		enrollmentDto.setCourseName("Java training");
		enrollmentDto.setEndDate(LocalDateTime.now());
		enrollmentDto.setStartDate(LocalDateTime.now());
		enrollmentDto.setStatus("INPROGRESS");
		enrollmentDto.setEnrollmentId(1);
		enrollmentDtoList.add(enrollmentDto);

		 responseDto = new EnrollmenrtResDto();
		responseDto.setStatusCode("200");
		responseDto.setMessage("success response");
		responseDto.setEnrollmentDtoList(enrollmentDtoList);
	
		responseError = new ResponseError();
		responseError.setMessage("user not found");
		responseError.setStatusCode(404);
			//responseDto.setConnectionRequestId(1);
	}

	@InjectMocks
	ViewEnrollmentsController viewEnrollmentsController;
	
	@Mock
	ViewEnrollmentsService viewEnrollmentsService;
	
	@Mock
	GlobalExceptionHandler globalExceptionHandler;
	
	@Mock
	EnrollmentDto enrollmentDto; 
	
	
	@Test
	public void viewEnrollments()
			throws  UserNotFoundException {
		
		

		
		Mockito.when(viewEnrollmentsService.findAllByUserIdGroupByStatus(1)).thenReturn(responseDto);
		
		ResponseEntity<EnrollmenrtResDto> result = viewEnrollmentsController.viewEnrollments(1);
	//	System.out.println(result.getStatusCodeValue());

		assertEquals(HttpStatus.OK, result.getStatusCode());
		//return result;

	}

	
	@Test
	public void viewEnrollmentsFail()
			throws  UserNotFoundException {
		
		

		
		Mockito.when(viewEnrollmentsService.findAllByUserIdGroupByStatus(1)).thenThrow(new UserNotFoundException("user not found"));
		
	//	ResponseEntity<EnrollmenrtResDto> result = viewEnrollmentsController.viewEnrollments(1);
	//	System.out.println(result.getStatusCodeValue());
		
		

		//assertNotEquals(HttpStatus.OK, result.getStatusCode());
		//return result;

	}

	

	

}
